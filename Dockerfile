FROM webdevops/php-nginx:ubuntu-14.04

EXPOSE 80

RUN cd ~ && wget https://files.magerun.net/n98-magerun.phar && \
 chmod +x ./n98-magerun.phar && \
 sudo cp ./n98-magerun.phar /usr/local/bin/

RUN rm -rf /var/lib/apt/lists/*

ENV APPLICATION_PATH=/var/www \
 WEB_DOCUMENT_ROOT=/var/www \
 CRYPT_KEY="" \
 DB_TABLE_PREFIX="" \
 DB_HOST="" \
 DB_USERNAME="" \
 DB_PASSWORD="" \
 DB_NAME="magento" \
 DB_INIT_STATEMENTS="SET NAMES utf8" \
 DB_MODEL="mysql4" \
 DB_TYPE="pdo_mysql" \
 DB_PDO_TYPE="" \
 SESSION_SAVE="files" \
 BASE_URL="" \
 SECURE_URL="" \
 ADMIN_USER_FIRST="" \
 ADMIN_USER_LAST="" \
 ADMIN_USER_EMAIL="" \
 ADMIN_USER_USERNAME="" \
 ADMIN_USER_PASSWORD="" \
 ADMIN_FRONT_NAME="admin"

# SSL Supprt
#EXPOSE 443
#COPY certificates/domain.com.crt /opt/docker/etc/httpd/ssl/server.crt
#COPY certificates/domain.com.key /opt/docker/etc/httpd/ssl/server.key
#COPY certificates/domain.com.csr /opt/docker/etc/httpd/ssl/server.csr

WORKDIR /var/www

COPY . /var/www

COPY app/etc/config.sh /
RUN chmod +x /config.sh
ENTRYPOINT /config.sh && /opt/docker/bin/entrypoint.sh supervisord