#!/bin/bash

set -e

###############################################
## Run MAGENTO SETUP from environment variables
###############################################

MAGENTO_ROOT=/var/www
LOCAL_XML_PATH="$MAGENTO_ROOT/app/etc/local.xml"
TEMPLATE="$LOCAL_XML_PATH.template"

if [ -z "${REDIS_CACHE_PORT}" ]
    then
    export REDIS_CACHE_PORT="6379"
fi

if [ -z "${REDIS_FPC_PORT}" ]
    then
    export REDIS_FPC_PORT="6379"
fi

if [ -z "${REDIS_FPC_ENDPOINT}" ] && [ -n "${REDIS_CACHE_ENDPOINT}" ]
    then
    export REDIS_FPC_ENDPOINT=${REDIS_CACHE_ENDPOINT}
fi

if [ -n "${REDIS_CACHE_ENDPOINT}" ]
    then
    TEMPLATE="$LOCAL_XML_PATH.redis.template"
fi


###############################################
## Install MAGENTO from environment variables
###############################################
# INSTALLED_SQL="SELECT value FROM core_config_data WHERE path = 'system/status/install'"
# INSTALLED_SQL_CLEAR="DELETE FROM core_config_data WHERE path = 'system/status/install'"
# INSTALLED_SQL_INSERT="INSERT INTO core_config_data (scope, scope_id, path, value) VALUES ('default', '0', 'system/status/install', 'complete')"

# set +e 
# CAN_CONNECT=0
# TRIES=0
# while [ $CAN_CONNECT -eq 0 ] && [ $TRIES -lt 6 ]
# do
#     MAGENTO_INSTALL_STATUS=$(mysql -h "${DB_HOST}" -u "${DB_USERNAME}" -p"${DB_PASSWORD}" "${DB_NAME}" -e "SHOW TABLES;" -s --skip-column-names)
#     if [ $? -eq 0 ]
#         then
#         CAN_CONNECT=1
#         else
#         sleep 5
#         TRIES=$((TRIES+1))
#     fi
# done
# set -e

# if [ "$MAGENTO_INSTALL_STATUS" != "complete" ] && [ -n "${ADMIN_USER_USERNAME}" ] && [ -n "${ADMIN_USER_PASSWORD}" ]
#     then
#     echo "Installing Magento via CLI...."
#     php install.php -- --license_agreement_accepted "yes" --locale "en_US" --timezone "America/Los_Angeles" \
#         --default_currency "USD" --db_host "${DB_HOST}" --db_name "${DB_NAME}" --db_user "${DB_USERNAME}" \
#         --db_pass "${DB_PASSWORD}" --db_prefix "" --session_save "${SESSION_SAVE}" --admin_frontname "${ADMIN_FRONT_NAME}" \
#         --url "${BASE_URL}" --secure_base_url "${SECURE_URL}" --admin_firstname "${ADMIN_USER_FIRST}" --admin_lastname "${ADMIN_USER_LAST}" \
#         --admin_email "${ADMIN_USER_EMAIL}" --admin_username "${ADMIN_USER_USERNAME}" --admin_password "${ADMIN_USER_PASSWORD}" \
#         --use_rewrites "yes" --use_secure "yes" --use_secure_admin "yes" --skip_url_validation "yes" \
#         --encryption_key "${CRYPT_KEY}"
    
#     echo "Updating the DB with Install Status"
#     mysql -h "${DB_HOST}" -u "${DB_USERNAME}" -p"${DB_PASSWORD}" "${DB_NAME}" -e "$INSTALLED_SQL_INSERT"

#     else
#     echo "Magento Aleady Marked Install Status: Complete"

# fi


###############################################
## Create MAGENTO local.xml from environment variables
###############################################

cp $TEMPLATE "$LOCAL_XML_PATH.processing"
sed -i 's/{{key}}/<![CDATA['"${CRYPT_KEY}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{date}}/<![CDATA['"$(date)"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{db_prefix}}/<![CDATA['"${DB_TABLE_PREFIX}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{db_host}}/<![CDATA['"${DB_HOST}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{db_user}}/<![CDATA['"${DB_USERNAME}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{db_pass}}/<![CDATA['"${DB_PASSWORD}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{db_name}}/<![CDATA['"${DB_NAME}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{db_init_statemants}}/<![CDATA['"${DB_INIT_STATEMENTS}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{db_model}}/<![CDATA['"${DB_MODEL}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{db_type}}/<![CDATA['"${DB_TYPE}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{db_pdo_type}}/<![CDATA['"${DB_PDO_TYPE}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{session_save}}/<![CDATA['"${SESSION_SAVE}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{admin_frontname}}/<![CDATA['"${ADMIN_FRONT_NAME}"']]>/g' "$LOCAL_XML_PATH.processing"

sed -i 's/{{redis_cache_endpoint}}/<![CDATA['"${REDIS_CACHE_ENDPOINT}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{redis_cache_port}}/<![CDATA['"${REDIS_CACHE_PORT}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{redis_fpc_endpoint}}/<![CDATA['"${REDIS_FPC_ENDPOINT}"']]>/g' "$LOCAL_XML_PATH.processing"
sed -i 's/{{redis_fpc_port}}/<![CDATA['"${REDIS_FPC_PORT}"']]>/g' "$LOCAL_XML_PATH.processing"

cp "$LOCAL_XML_PATH.processing" "$LOCAL_XML_PATH"

echo "Magento Configuration Written"


###############################################
## Clear Cache and Setup MAGENTO file permissions
###############################################

rm -rf "$MAGENTO_ROOT/var/cache"

echo "Cache Cleared"

echo "Updating Ownership of media/var/etc"
chown -R www-data:www-data "$MAGENTO_ROOT/media"
chown -R www-data:www-data "$MAGENTO_ROOT/var"
chown -R www-data:www-data "$MAGENTO_ROOT/app/etc"

echo "Updating Permission for media/var"
find "$MAGENTO_ROOT/media" -type d | xargs chmod 777
find "$MAGENTO_ROOT/media" -type f | xargs chmod 666
find "$MAGENTO_ROOT/var" -type d | xargs chmod 777
find "$MAGENTO_ROOT/var" -type f | xargs chmod 666
find "$MAGENTO_ROOT/app/etc" -type d | xargs chmod 777
find "$MAGENTO_ROOT/app/etc" -type f | xargs chmod 666