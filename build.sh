#!/bin/bash

#
# BUILD
#
#
function build {
    docker build -t yp .
    docker tag yp:latest 005169657771.dkr.ecr.us-east-1.amazonaws.com/yp:latest
    echo "Image is Built and tagged, would you like to push? (y/n)"
    say "Image is Built and has been tagged... Would you like to Push the Image?"
    read PUSHPROMPT
    if [ "$PUSHPROMPT" = "y" ]
        then
        push
    fi
}

function push {
    $(aws ecr get-login --region us-east-1 --profile yp)
    docker push 005169657771.dkr.ecr.us-east-1.amazonaws.com/yp
    say "YP... Image is pushed."
    docker rmi $(docker images | grep 005169657771.dkr.ecr.us-east-1.amazonaws.com/yp | awk '{print $3}')
}


COMMAND=$1

if [ -z "$COMMAND" ]
    then 
    echo " *** HELP ****************** "
    echo " "
fi

$COMMAND $2 $3 $4