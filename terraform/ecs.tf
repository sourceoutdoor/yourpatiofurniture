resource "aws_ecs_cluster" "yp-production" {
  name = "yp-production"
}

resource "aws_ecs_task_definition" "web" {
  family = "web"
  container_definitions = "${file("ecs/web-container.json")}"

  volume {
    name = "web-media"
    host_path = "/ecs/web-media"
  }
}