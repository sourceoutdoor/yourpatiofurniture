<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'magento');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '1');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%BB#UAX{;ZCua)RdKWWSju>pqER*edQl{Y^vpt*,~0)03sXrTxZgxX7X/^.KJ^NB');
define('SECURE_AUTH_KEY',  '8.<wzq^rls?.plh$/8E^~Co-ei3|l<-bP6Gn|o&ji$2e]?&.n:#QM`V:. J#3|lF');
define('LOGGED_IN_KEY',    '/cjfqZ@Xst:w1$DIGOTn u?awZV7[h0D[?(,[tu!YN]}#`b#b #`&$FIC+<U`kVS');
define('NONCE_KEY',        'L+heYGg!MmX*wzYo?C$mLd[}pU~VDmg1uRak][2,9aThpm UYH*h^T~C]pwvO<%@');
define('AUTH_SALT',        '?J$x=t0X[f=PJzNu2&dtETf4wDXe7Yu_JjTna+%76|W4razi3/gRx_v`:Zl@l!B<');
define('SECURE_AUTH_SALT', 'NC!]33V#9f@LijVbD[ZFMp;aDD>jpK`bu|WG?K=LU=kTyd.-%&mE:Tz-_vV*N3Rf');
define('LOGGED_IN_SALT',   'wkt9RX$:6%(*&2.61!]d=hRF?tT14`=heM;r a:S1l0b,RU}D[z`G_2p#lCoD)I~');
define('NONCE_SALT',       'JojQdmj-T~^^OB`U_H1@2KE22HQ}HR+%R,~4xklA|m:sWtSEfB$]I&Em+jr%){~_');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
